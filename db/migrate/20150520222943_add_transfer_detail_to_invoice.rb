class AddTransferDetailToInvoice < ActiveRecord::Migration
  def change
    add_column :invoices, :transfer_detail, :text
  end
end
