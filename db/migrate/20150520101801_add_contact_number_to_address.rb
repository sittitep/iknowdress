class AddContactNumberToAddress < ActiveRecord::Migration
  def change
    add_column :addresses, :contact_number, :string
  end
end
