class AddOnDemandToVariant < ActiveRecord::Migration
  def change
    add_column :variants, :on_demand, :boolean
  end
end
