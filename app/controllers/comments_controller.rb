class CommentsController < ApplicationController
  before_filter :prev_path

  def create
    Comment.create comment_params

    flash[:notice] = "ระบบได้รับความคิดเห็นของคุณแล้ว ขอบคุณค่ะ"
    redirect_to session.delete(:return_to)
  end

  def destroy
    comment = Comment.find params[:id]
    comment.destroy

    redirect_to session.delete(:return_to)
  end

  private
    def comment_params
      params.require(:comment).permit!
    end

    def prev_path
      session[:return_to] ||= request.referer
    end
end
