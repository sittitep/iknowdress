class LikeItemsController < ApplicationController
  before_filter :require_user

  def create
    product = Product.find params[:product_id]
    if current_user.like?(product)
      product.unliked_by current_user
    else
      product.liked_by current_user
    end
    
    render nothing: true
  end
end
