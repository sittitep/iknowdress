class ProductsController < ApplicationController
  add_breadcrumb "สินค้าทั้งหมด", "/products"

  def index
    title_arr = ["เสื้อผ้า"]

    products = Product.active.includes(:variants)

    product_types = nil
    if params[:product_type_id].present? && product_type = ProductType.find_by_id(params[:product_type_id])
      product_types = product_type.self_and_descendants.map(&:id)
      product_type.all_path.each do |n|
        add_breadcrumb ProductType.find(n).name, products_path(product_type_id: n)
        title_arr << ProductType.find(n).name
      end
    end
    if product_types
      @products = products.where(product_type_id: product_types)
    
      
    elsif params[:q]
      ids = []
      keywords = params[:q][:description].split(" ")
      keywords.each do |keyword|
        products = products.where(id: ids) if ids.present?
        ids = ids + products.where("description like ?", "%#{keyword}%").ids
      end
      title_arr += keywords
      @products = products.where(id: ids)
      add_breadcrumb "ผลการค้าหา #{keywords.join(",")}", "/products"
    else
      @products = products
    end
    
    @products = @products.paginate(:page => params[:page], :per_page => 24)
    set_meta_tags :title => title_arr.join(" ")
  end

  def create
    if params[:q] && params[:q].present?
      @products = Product.standard_search(params[:q]).results
    else
      @products = Product.where('deleted_at IS NULL OR deleted_at > ?', Time.zone.now )
    end

    render :template => '/products/index'
  end

  def show
    @product = Product.active.find(params[:id])

    set_meta_tags :title => @product.name
    set_meta_tags :keywords => @product.set_keywords
    set_meta_tags :description => @product.description

    @product.product_type.all_path.each do |n|
      add_breadcrumb ProductType.find(n).name, products_path(product_type_id: n)
    end
    add_breadcrumb @product.name, product_path(@product)

    form_info
    @cart_item.variant_id = @product.active_variants.first.try(:id)
  end

  private

  def form_info
    @cart_item = CartItem.new
  end

  def featured_product_types
    [ProductType::FEATURED_TYPE_ID]
  end
end
