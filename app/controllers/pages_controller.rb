class PagesController < ApplicationController
	def about
		add_breadcrumb "เกี่ยวกับเรา", "/about"
	end

	def terms
		add_breadcrumb "เงื่อนไข", "/term"
		set_meta_tags :title => "เงื่อนไข"
	end

	def how_to_order
		add_breadcrumb "วิธีสั่งสินค้า", "/how-to-order"
		set_meta_tags :title => "วิธีสั่งสินค้า"
	end

	def how_to_notify
		
	end

	def faq
		
	end

	def contact
		
	end
end
