class Myaccount::OverviewsController < Myaccount::BaseController
  add_breadcrumb "บัญชีของคุณ", :myaccount_overview_path
  def show
    @orders = current_user.finished_orders.find_myaccount_details
    @orders = @orders.reverse
  end

  def edit
    add_breadcrumb "แก้ไขข้อมูลส่วนตัว", :edit_myaccount_overview_path
    @user = current_user
  end

  def update
    @user = current_user
    if @user.update_attributes(user_params)
      redirect_to myaccount_overview_url(), :notice  => "Successfully updated user."
    else
      render :edit
    end
  end

  def like_items
    @products = (current_user.get_voted Product).paginate(:page => params[:page], :per_page => 24)
    add_breadcrumb "สินค้าที่คุณชอบ", :like_items_myaccount_overview_path
  end

  private

    def user_params
      params.require(:user).permit(:email, :password, :password_confirmation, :first_name, :last_name)
    end
    def selected_myaccount_tab(tab)
      tab == 'profile'
    end
end
