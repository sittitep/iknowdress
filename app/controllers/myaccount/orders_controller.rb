class Myaccount::OrdersController < Myaccount::BaseController
  # GET /myaccount/orders
  # GET /myaccount/orders.xml
  def index
    @orders = current_user.finished_orders.find_myaccount_details
  end

  # GET /myaccount/orders/1
  # GET /myaccount/orders/1.xml
  def show
    @order = current_user.finished_orders.includes([:invoices]).find_by_number(params[:id])
  end

  def new_transfer_verification
    @order = Order.where(id: params[:id]).first
    invoice = @order.invoices.first
    @tv = TransferVerification.new(invoice_id: invoice.id)

    render layout: "pop_up"
  end

  def transfer_verification
    arr = []
    params[:transfer_verification].each do |k,v|
      arr << v if !["invoice_id","time(1i)","time(2i)","time(3i)"].include?(k)
    end
    transfer_detail = arr.join("/")
    invoice = Invoice.find params[:transfer_verification][:invoice_id]
    invoice.update_attributes(transfer_detail: transfer_detail)

        
    # if invoice.confirm_transfer
      
    # else
    #   logger.info "######################### auto match transfer failed"
    # end

    flash[:notice] = "การแจ้งยอดเสร็จสมบุรณ์แล้ว"
    redirect_to myaccount_overview_path
  end

  private

  def selected_myaccount_tab(tab)
    tab == 'orders'
  end


end
