class Admin::CommentsController < Admin::BaseController
	def index
		@comments = Comment.all
	end

	def  update
		comment = Comment.find params[:id]
		comment.update_attributes params_comment

		redirect_to(:back)
	end

	private
		def params_comment
			params.require(:comment).permit!
		end
end