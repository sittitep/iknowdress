class UserSessionsController < ApplicationController
  before_filter :is_login?, only: [:new, :create]

  def new
    @user_session = UserSession.new
    @user = User.new
  end

  def create
    if params[:user_session]["email"] == "guest"
      user_params = create_guest_user
    end

    @user_session = UserSession.new(user_params || params[:user_session])
    if @user_session.save
      cookies[:hadean_uid] = @user_session.record.access_token
      session[:authenticated_at] = Time.now
      ## if there is a cart make sure the user_id is correct
      set_user_to_cart_items(@user_session.record)
      merge_carts
      if @user_session.record.admin?
        redirect_back_or_default admin_users_url
      else
        redirect_back_or_default root_url
      end
    else
      @user = User.new(user_params)
      redirect_to login_url, :alert => I18n.t('login_failure')
    end
  end

  def destroy
    current_user_session.destroy
    reset_session
    cookies.delete(:hadean_uid)
    redirect_to login_url
  end

  private

  def user_params
    params.require(:user_session).permit(:password, :password_confirmation, :first_name, :last_name, :email)
  end

  def create_guest_user
    begin
      email = "guest#{rand(1..100000)}@iknowdress.com"
    end until User.where(email: email).blank?

    password = rand(100000000..1000000000)

    user = User.create(email: email,
                       first_name: "guest",
                       last_name: "guest",
                       password: password)
    json = user.as_json
    json["password"] = password

    return json
  end

  def is_login?
    if current_user.present?
      current_user_session.destroy
      reset_session
      cookies.delete(:hadean_uid)
      redirect_to login_url
    end
  end 
end
