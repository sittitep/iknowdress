//= require jquery
//= require jquery-ui
//= require jquery_ujs
//= require jquery.bxslider
//= require jquery.magnific-popup
//= require twitter/bootstrap
//= require_tree  ./jquery
//= require_tree  ./layout
//= require foundation
var clientValidate = function(){
	var form = $("form.client-validate")
	form.on("submit", function(){

		var inputs = $(this).find(".required")
		var error = false

		$.each(inputs, function(){
			if($(this).val() == ""){
				$(this).addClass("error")
				$(this).prev().addClass("error")
				error = true
			}
		})

		if(error){
			return false
		}
	})
}

$(function(){ $(document).foundation(); });
$("document").ready(function(){
	clientValidate()

	$('.bxslider').bxSlider({
	  pagerCustom: '#bx-pager',
	  controls: false,
	  responsive: false
	});
	$('.verify-transfer').magnificPopup({ 
    type: 'ajax',
 		midClick: true,
 		closeOnBgClick: false,
 		closeOnContentClick: false
  });

  var $easyzoom = $('.easyzoom').easyZoom();

  $("a.heart").on("click", function(){
  	if($(this).hasClass("active")){
  		$(this).removeClass("active")
  	}else{
  		$(this).addClass("active")
  	}
  })
})